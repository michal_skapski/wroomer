﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponBooster : MonoBehaviour
{
    [SerializeField] private GameObject _thisObject;
    [SerializeField] private float _timeToDestroy = 0f;
    [SerializeField] private bool _mini = false;
    [SerializeField] private bool _rocket = false;
    [SerializeField] private bool _sniper = false;
    [SerializeField] private bool _slapper = false;
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<AltCarMove>())
        {
            AltCarMove player = other.GetComponent<AltCarMove>();
            if (_mini == true)
            {
                player._hasMinigun = true;
                player.GainWeapon(0);
            }
            if (_rocket == true)
            {
                player._hasRocketLauncher = true;
                player.GainWeapon(1);
            }
            if (_sniper == true)
            {
                player._hasSniperRifle = true;
                player.GainWeapon(2);
            }
            if (_slapper == true)
            {
                player._hasSlapper = true;
                player.GainWeapon(3);
            }
            Destroy(_thisObject, 0);
        }
    }
}
