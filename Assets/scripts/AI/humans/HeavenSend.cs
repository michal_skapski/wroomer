﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeavenSend : MonoBehaviour
{
    [SerializeField] private GameObject _lilHuman;
    private Animator anim;
    void Start()
    {
        anim = GetComponent<Animator>();
        _lilHuman.GetComponent<LilHum>();
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<AltCarMove>())
        {
            DieInnocentOne();
            //Debug.Log("HehsCar");
        }
        else if (other.GetComponent<Destroy>())
        {
            DieInnocentOne();
            //Debug.Log("HehsPro");
        }
        else if (other.GetComponent<HP>())
        {
            DieInnocentOne();
            //Debug.Log("HehsCarzie");
        }
        else if (other.GetComponent<Explosion>())
        {
            DieInnocentOne();
            //Debug.Log("Oh, cmon!");
        }
    }
    void DieInnocentOne()
    {
        anim.SetBool("RunOver", true);
        _lilHuman.GetComponent<LilHum>().Die();
    }
}
