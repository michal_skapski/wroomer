﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LilHum : MonoBehaviour
{
    public Transform target;
    private Rigidbody rb;
    [SerializeField] private GameObject _thisAI;
    [SerializeField] private float _speed = 1f;
    [SerializeField] private float _maxSpeed = 200f;
    [SerializeField] private float _rotSpeed = 0.05f;
    [SerializeField] private int _distToNotice = 20;
    private bool _destroyed = false;
    private bool _gaveScore = false; // for substracting score purpose
    [SerializeField] private int points = -1;
    private GameObject _menu;
    private Menu _menuRef;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        _menu = Menu._thisStaticMenu;
        _menuRef = _menu.GetComponent<Menu>();
    }
    void Update()
    {
        rb.velocity = Vector3.ClampMagnitude(rb.velocity, _maxSpeed);
        if (_destroyed == false)
        {
             Roam();
        }
    }
    private void Roam()
    {
        if (target != null)
        {
            Vector3 direction = target.position - this.transform.position;
            float angle = Vector3.Angle(direction, this.transform.forward);
            if (Vector3.Distance(target.position, this.transform.position) < _distToNotice)// && angle < visionAngle
            {
                direction.y = 0;
                this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
                    Quaternion.LookRotation(-direction), _rotSpeed);
                    Move();
            }
        }
    }
    private void Move()
    {
        rb.AddForce(_thisAI.transform.forward * _speed * Time.deltaTime, ForceMode.Acceleration);
    }
    private void OnTriggerEnter(Collider other)
    {
       target = other.transform;
    }
    public void Die()
    {
        _destroyed = true;
        if (_gaveScore == false)
        {
            _menuRef.AddScore(points);
            _gaveScore = true;
        }
    }
}
