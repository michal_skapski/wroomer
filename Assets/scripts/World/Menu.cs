﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    [SerializeField] private GameObject _thisMenu;
    [SerializeField] private Transform _compass;
    private void Start()
    {
        AssignMenu();
    }
    //add listener onclick
    public void GoToMenu()
    {
        SceneManager.LoadScene(0);
    }
    public void PlayGame()
    {
        SceneManager.LoadScene(1);
    }
    public void ExitGame()
    {
        Application.Quit();
    }
    public void AssignMenu()
    {
        AltCarMove.menuManager = _thisMenu;
        AltCarMove.otherObject = _compass;
    }
}
