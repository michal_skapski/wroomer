﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float speed = 10.0f;
    public int damage = 2;
    [SerializeField] private GameObject _burst;
    void Update()
    {
        transform.Translate(0, 0, speed * Time.deltaTime);
    }
    void OnTriggerEnter(Collider other)
    {
        GameObject projectile = Instantiate(_burst) as GameObject;
        projectile.transform.position = this.transform.position;
        //creating sparks on hit with other collider, when pooling is investigated check if it can be augmented here
        HP player = other.GetComponent<HP>();
        if (player != null)
        {
            player.TakeDamage(damage);
        }
        Destroy(this.gameObject);
    }
}
