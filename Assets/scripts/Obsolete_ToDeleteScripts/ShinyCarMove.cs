﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShinyCarMove : MonoBehaviour
{
    [SerializeField] private float MouseSensitivity;
    [SerializeField] private float MoveSpeed;
    [SerializeField] private Transform _frontWheelz;
    [SerializeField] private float rotSpeed;
    private Rigidbody rb;
    float mouseX, mouseY;
    //private Animator anim;
    //private Collider coll;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        //    coll = GetComponent<Collider>();
        //    anim = GetComponent<Animator>();
    }
    void Update()
    {
        mouseY = Mathf.Clamp(mouseY, -30, 30);
        mouseX = Mathf.Clamp(mouseX, -30, 30);
        //mouseX += Input.GetAxis("Mouse X") * rotSpeed;
        //mouseY -= Input.GetAxis("Mouse Y") * rotSpeed;

        _frontWheelz.rotation = Quaternion.Euler(mouseY, mouseX, 0);
        if (Input.GetKey(KeyCode.W) || (Input.GetKey(KeyCode.S)))   // moving
        {
            rb.MovePosition(_frontWheelz.transform.position +
                (_frontWheelz.transform.forward * Input.GetAxis("Vertical") * MoveSpeed * Time.deltaTime));
            //var step = rotSpeed * Time.deltaTime;
            //this.transform.rotation = Quaternion.RotateTowards(transform.rotation, _frontWheelz.rotation, step);
            //if (Input.GetKey(KeyCode.A) || (Input.GetKey(KeyCode.D)))
            //{
            //    Quaternion deltaRot = Quaternion.Euler(0, rotSpeed, 0);
            //    rb.MoveRotation(rb.rotation * deltaRot);
                //rb.MoveRotation(rb.rotation * Quaternion.Euler(new Vector3(0, rotSpeed, 0)));
            //}
        }
        if (Input.GetKey(KeyCode.D))
        {
            _frontWheelz.transform.Rotate(0, rotSpeed, 0);
        }
        if (Input.GetKey(KeyCode.A))
        {
            _frontWheelz.transform.Rotate(0, -rotSpeed, 0);
            //Quaternion deltaRot = Quaternion.Euler(0, rotSpeed, 0);
            //rb.MoveRotation(rb.rotation * deltaRot);
        }

    }
}
